﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Hello world"); // see on jutu algus
            Console.Write("Mis värvi tuli on valgusfooris: ");
            string tuli = Console.ReadLine();

            switch (tuli.ToLower())
            {
                case "punane":
                case "p":
                    Console.WriteLine("jää nüüd seisma");
                    break;
                case "roheline":
                case "r":
                    Console.WriteLine("võid sõita");
                    break;
                case "kollane":
                case "k":
                    Console.WriteLine("ole valmis seisma jääma");
                    break;
                default:
                    Console.WriteLine("osta prillid");
                    break;
            }

            


            // sama asi IF lausega (kontrollin esimest tähte)

            /* see on variant IF lausega

            if (tuli.ToLower().Substring(0, 1) == "p") Console.WriteLine("jää kohe seisma");
            else
            if (tuli.ToLower().Substring(0, 1) == "r") Console.WriteLine("sõida edasi");
            else
            if (tuli.ToLower().Substring(0, 1) == "k") Console.WriteLine("ole valmis seisma jääma");
            else
                Console.WriteLine("osta prilllid");

            */

        }
    }
}
